<!DOCTYPE html>
<html>
    <head>
        <title>CTOOL v1.0</title>
        <meta charset="utf-8">

        <!-- Bootstrap CSS, site CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row tool-section">
                <div class="col-5">
                    <p>CTOOL</p>
                </div>
                <div class="col-2">
                    <span>Layout Name</span>
                    <input class="layout-file-name" type="text" value=""/>
                </div>
                <div class="col-5">
                    <button type="button" class="btn btn-secondary" onclick="show_layout_json()">Show JSON</button>
                    <!-- <button type="button" class="btn btn-secondary">Import JSON</button> -->
                    <button type="button" class="btn btn-secondary" onclick="export_layout_json()">Export JSON</button>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <div>
                        <div class="canvas">
                            <?php for($j=0; $j<8; $j++){?>
                            <div class="canvas-box box-<?php echo $j ?>">

                            </div>
                            <?php }?>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="sections-list">
                        <?php for($i=0; $i<8; $i++) { ?>
                        <div class="sections-list-item sli-<?php echo $i ?>">
                            <div class="sli-num"><?php echo $i ?></div>
                            <div class="sli-colbox"></div>
                            <div class="sli-coord">
                                <!-- Section List Item Value Fields -->
                                <input 
                                    class="slivf sli-x" type="number" value="0" 
                                    onchange="update_layout_boxes(<?php echo $i ?>)">
                                <input 
                                    class="slivf sli-y" type="number" value="0" 
                                    onchange="update_layout_boxes(<?php echo $i ?>)">
                                <input 
                                    class="slivf sli-w" type="number" value="0" 
                                    onchange="update_layout_boxes(<?php echo $i ?>)">
                                <input 
                                    class="slivf sli-h" type="number" value="0" 
                                    onchange="update_layout_boxes(<?php echo $i ?>)">
                            </div>
                            <div class="sli-vis">
                                <span>
                                    <label>
                                        <input 
                                            class="sli-enabed" 
                                            type="checkbox" 
                                            onclick="sli_enabled_checkbox_changed(this)"
                                            data-slienum="<?php echo $i ?>"> Enabled?
                                    </label>
                                </span>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <br>
                    <div>
                        <p>Some constants</p>
                        <p>0.33333333333333</p>
                        <p>0.33333333333334</p>
                        <p>0.66666666666664</p>
                        <p>0.66666666666666</p>
                        <p>0.66666666666667</p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="layout-json-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Show JSON</h5>
                    <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <pre>
                        <div class="layout-json-modal-body"></div>
                    </pre>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>

        <!-- jQuery, Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="bootstrap.min.js"></script>
        <script src="script.js"></script>
    </body>
</html>