const constants = {
    color_array: [
        'red',
        'orange',
        'yellow',
        'lawngreen',
        'blue',
        'magenta',
        'pink',
        'indigo'
    ]
};
var layout = {
    "sections": [
        {
            "x": 0.0,
            "y": 0.0,
            "w": 0.0,
            "h": 0.0,
            "subgrids": []
        }
    ]
};

$(document).ready(function(){
    // Color the canvas boxes
    $(".canvas-box").each(function(i, v){
        $(v).css('border-width', '5px');
        $(v).css('border-color', constants.color_array[i]);
        $(v).css('border-style', 'solid');
        $(v).css('width', (i*10) + 'px');
        $(v).css('height', (i*10) + 'px');
        $(v).css('z-index', i);
    });

    // Color the section list item color indicators
    $(".sli-colbox").each(function(i, v){
        $(v).css('background-color', constants.color_array[i]);
    });
});

function downloadFile(content) {
    let fileName = $(".layout-file-name").val() + ".json";
    var a = document.createElement("a");
    var file = new Blob([content], {type: 'text/plain'});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

function sli_enabled_checkbox_changed(e){
    let checkbox = $(e);
    let checkbox_index = checkbox.data("slienum");
    let checkbox_is_checked = checkbox.prop("checked");
    console.log("A checkbox changed checkboxid", checkbox_index, "checked?", checkbox_is_checked);
    set_canvas_box_visibility(checkbox_index, checkbox_is_checked);
    update_layout_boxes(checkbox_index);
}

function set_canvas_box_visibility(index, is_shown){
    let target_box_class = "." + "box-" + index;
    let target_visibility_css = is_shown ? "visible" : "hidden";
    $(target_box_class).css('visibility', target_visibility_css);
}

function update_layout_boxes(index){

    let target_coord_div = ".sli-" + index + "> .sli-coord > ";

    let target_box_class = "." + "box-" + index;

    let x_value = $(target_coord_div + ".sli-x").val() % 2.0;
    let y_value = $(target_coord_div + ".sli-y").val() % 2.0;
    let w_value = $(target_coord_div + ".sli-w").val() % 2.0;
    let h_value = $(target_coord_div + ".sli-h").val() % 2.0;

    $(target_box_class).css('top', (y_value * 100.0) + "%");
    $(target_box_class).css('left', (x_value * 100.0) + "%");
    $(target_box_class).css('width', (w_value * 100.0) + "%");
    $(target_box_class).css('height', (h_value * 100.0) + "%");
}

function update_layout_json(){
    let enabled_indexes = $(".sli-enabed:checked").map(function(i, v){
        console.log($(v).data("slienum"));
        return $(v).data("slienum");
    });
    // console.log(enabled_indexes);

    if (enabled_indexes.length == 0){
        alert("No boxes enabled. Please enable some boxes and try again.");
        return
    }

    var sections = [];

    enabled_indexes.each(function(i, v){
        let target_coord_div = ".sli-" + i + "> .sli-coord > ";
        let x_value = $(target_coord_div + ".sli-x").val();
        let y_value = $(target_coord_div + ".sli-y").val();
        let w_value = $(target_coord_div + ".sli-w").val();
        let h_value = $(target_coord_div + ".sli-h").val();
        let obj = {
            "x": parseFloat(x_value),
            "y": parseFloat(y_value),
            "w": parseFloat(w_value),
            "h": parseFloat(h_value),
            "subgrids": []
        }

        sections.push(obj);
    });

    layout = {
        "sections": sections
    };
}

function show_layout_json(){
    update_layout_json();
    $('.layout-json-modal-body').html(JSON.stringify(layout, null, 2));
    $('#layout-json-modal').modal('show');
}

function export_layout_json(){
    update_layout_json();
    let final_layout_file = {
        "numberOfSections": layout.sections.length,
        "name": $(".layout-file-name").val(),
        "sections": layout.sections
    };
    downloadFile(JSON.stringify(final_layout_file, null, "\t"));
}