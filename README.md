# Collage Tool

This contains the sources for the collage tool. It creates collage layouts for the Helakuru Photo Editor.

![Collage Tool in Action](Screenshot.png)

## Features ##
- [x] Export layout to JSON
- [x] Preview layout JSON
- [x] Visual preview of the current Layout
- [x] Add up to 8 individual layout sections

## Disclaimer ##

This tool was created as part of the Bhasha Helakuru PhotoEditor - Collage Layouts project.